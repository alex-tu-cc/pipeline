#!/bin/bash
# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

# Add an extra apt repository sources.list if the extra-repository env vars were
# set.

TARGET_ETC="/etc"
VERBOSE=0

while [[ "$#" -ge 1 ]]; do
    case "$1" in
        --target-etc|-t)
            shift
            TARGET_ETC="$1"
            shift
            ;;
        --verbose|-v)
            VERBOSE=1
            shift
            ;;
    esac
done

if [[ "$VERBOSE" -ne 0 ]]; then
    set -x
fi

mkdir -p "${TARGET_ETC}"/apt/sources.list.d/
index=0
list="${SALSA_CI_EXTRA_REPOSITORY}"
while [ -n "${list}" ]; do
    list="$(echo "${list}" | grep -v '^ *\(#.*\)\?$')"
    case "${list}" in
    "deb "*|"deb-src "*)
        echo "${list}" >> "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
        ;;
    "ppa:"*)
        if [ -n "$(which add-apt-repository || true)" ]; then
             for ppa in ${list}; do
                 add-apt-repository --yes --no-update "${ppa}";
             done
        fi
        ;;
    *)
        if [ -n "${list}" ]; then
            cat "${list}" >> "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
        fi
        ;;
    esac

    index=$((index+1))
    name=SALSA_CI_EXTRA_REPOSITORY_${index}
    list="${!name}"
done

mkdir -p "${TARGET_ETC}"/apt/trusted.gpg.d/
index=0
key="${SALSA_CI_EXTRA_REPOSITORY_KEY}"
while [ -n "${key}" ]; do
    if { echo "${key}"; } | grep -q 'BEGIN PGP PUBLIC KEY BLOCK'; then
        echo "${key}" > "${TARGET_ETC}"/apt/trusted.gpg.d/extra_repository_${index}.asc
    else
        key="$(echo "${key}" | grep -v '^ *\(#.*\)\?$')"
        if [ -n "${key}" ]; then
            cat ${key} > "${TARGET_ETC}"/apt/trusted.gpg.d/extra_repository_${index}.asc
        fi
    fi

    index=$((index+1))
    name=SALSA_CI_EXTRA_REPOSITORY_KEY_${index}
    key="${!name}"
done

apt-get update
apt-get --assume-yes install ca-certificates
